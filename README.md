# Estimopener #

An iBeacon Enhanced Garage Door Opener with IOS, Arduino, and an Estimote iBeacon.

### What is it? ###

Some time back, the button on the wall in my garage broke. I took the button down, exposing the wires, and just crossed them to close the circuit when I wanted to open or close my garage door without using the wireless remote.

Eventually, I made a new circuit with a new button (and LED) to recreate the one that was broken. But that was not cool enough, so I grabbed an Arduino Uno, an Ethernet Shield, Xcode, and went to work. I created a web-server to run on the Arduino and then created an IOS app to consume the web-server.

Then came IOS and iBeacons. With an Estimote developer kit in hand, I modified the IOS app to detect entering and exiting the beacon region and post local notifications to the device reminding me that I needed to open or close my garage door.

Someone asked about this very concept a few weeks back on Twitter. This code and the corresponding video show how I did it and how you can too.

Check out the video on [YouTube](http://youtu.be/27p6lhpPeLY). Don't forget to subscribe to our [YouTube Channel (BCGLLCTV)](http://www.youtube.com/user/bcgllctv) to keep up with this and other projects.

### Okay, so how do I use it? ###

* Connect the Arduino and Ethernet Shield
* Prepare the circuit based on the supplied Fritzing (circuit) diagram
* Download the Arduino Sketch and upload it to an Arduino
* Download the IOS code, build, and run
* Configure the iBeacon to use the proximityUUID, major, and minor as specified in the IOS app
* Have fun

### Brought to you by ###

Jonathan Burris - [Burris Consulting Group LLC](http://burrisconsultinggroup.com)

Feel free to email [jb@burrisconsultinggroup.com](mailto:jb@burrisconsultinggroup.com) with questions or comments
//
//  ViewController.h
//  Estimopener
//
//  Created by Jonathan Burris on 5/12/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

- (IBAction)activateDoor:(id)sender;

@end

//
//  ViewController.m
//  Estimopener
//
//  Created by Jonathan Burris on 5/12/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "ViewController.h"
#import "CoreLocationManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(approachBeacon:) name:kNotificationBeaconDistanced object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leaveBeacon:) name:kNotificationBeaconApproached object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doorActivated:) name:kNotificationDoorActivated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doorFailed:) name:kNotificationDoorFailed object:nil];

    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBeaconApproached object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBeaconDistanced object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationDoorActivated object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationDoorFailed object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.statusLabel.text = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)activateDoor:(id)sender {
    
    self.statusLabel.text = @"Sending signal...";
    
    [[CoreLocationManager sharedInstance] activateDoor];
    
}

- (void)approachBeacon:(id)sender {
    
    self.statusLabel.text = @"It is time to open your door.";
    
}

- (void)leaveBeacon:(id)sender {
    
    self.statusLabel.text = @"Don't forget to close the door!";
    
}

- (void)doorActivated:(id)sender {
    
    self.statusLabel.text = @"Door has been activated.";
    
}

- (void)doorFailed:(id)sender {
    
    self.statusLabel.text = @"Door could not be activated.";
    
}
@end

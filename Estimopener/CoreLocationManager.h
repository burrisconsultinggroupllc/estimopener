//
//  CoreLocationManager.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/25/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#define kBeaconRegionProximityUUID      @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
#define kBeaconRegionIdentifier         @"com.burrisconsultinggroup.estimopener"

#define kNotificationBeaconApproached   @"NotificationBeaconApproached"
#define kNotificationBeaconDistanced    @"NotificationBeaconDistanced"
#define kNotificationDoorActivated      @"NotificationDoorActivated"
#define kNotificationDoorFailed         @"NotificationDoorFailed"

static NSString *responseDoorActivated = @"Your door has been activated.";
static NSString *responseDoorFailed = @"Your door could not be activated.";

typedef NS_ENUM(NSInteger, BWLocalNotificationMessage) {
    BWLocalNotificationMessageBeaconApproached,
    BWLocalNotificationMessageBeaconDistanced
};

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CoreLocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLBeacon* lastBeacon;

+ (CoreLocationManager*)sharedInstance;

- (void)startMonitoringForRegion;

- (BOOL)locationServicesEnabled;

- (NSString *)stringForProximity:(CLProximity)proximity;

- (void)activateDoor;

@end

//
//  CoreLocationManager.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/25/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "CoreLocationManager.h"
#import "AFNetworking.h"

@interface CoreLocationManager()

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;
@property (nonatomic, assign) double lastDistance;

@end

@implementation CoreLocationManager

@synthesize locationManager = _locationManager;
@synthesize beaconRegion = _beaconRegion;
@synthesize lastBeacon = _lastBeacon;
@synthesize lastDistance = _lastDistance;

+(CoreLocationManager*)sharedInstance {
    
    static CoreLocationManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

-(id)init {
    
    if (self = [super init]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
        _beaconRegion = [self getBeaconRegion];
        
    }
    
    return self;
    
}

- (CLBeaconRegion*)getBeaconRegion {
    
    if (!self.beaconRegion) {
        
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:kBeaconRegionProximityUUID];
        NSInteger major = [[NSNumber numberWithInt:32] integerValue];
        
        _beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major minor:14760 identifier:kBeaconRegionIdentifier];
        _beaconRegion.notifyOnEntry = YES;
        _beaconRegion.notifyOnExit = YES;
        _beaconRegion.notifyEntryStateOnDisplay = YES;
        
    }
    
    return self.beaconRegion;
    
}

- (void)startMonitoringForRegion {
    
    if (![self locationServicesEnabled])
        return;
    
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
    
    //[self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    [self postLocalNotification:BWLocalNotificationMessageBeaconApproached];
    
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    [self postLocalNotification:BWLocalNotificationMessageBeaconDistanced];
    
}

//- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
//    
//    if (![region.identifier isEqualToString:kBeaconRegionIdentifier])
//        return;
//    
//    CLBeacon *thisBeacon = [beacons firstObject];
//    
//    if (thisBeacon.accuracy > 0 && self.lastBeacon.accuracy > 0) {
//        
//        double distance = thisBeacon.accuracy - self.lastBeacon.accuracy;
//        
//        //Going away from beacon
//        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBeaconChanged object:self];
//        
//        _lastDistance = distance;
//        
//    }
//    
//    _lastBeacon = thisBeacon;
//    
//}

- (BOOL)locationServicesEnabled {
    
    BOOL enabled = NO;
    
    if ((![CLLocationManager locationServicesEnabled])
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)) {
        enabled = NO;
    } else {
        enabled = YES;
    }
    
    return enabled;
    
}

- (void)postLocalNotification:(BWLocalNotificationMessage)localNotificationMessage {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.userInfo = @{@"identifier": self.beaconRegion.identifier};
    notification.soundName = @"Default";
    
    if (localNotificationMessage == BWLocalNotificationMessageBeaconApproached) {
        notification.alertBody = @"Welcome home. Do you need to open your door?";
    } else if (localNotificationMessage == BWLocalNotificationMessageBeaconDistanced) {
        notification.alertBody = @"Don't forget to close your door!";
    }
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
}

- (NSString *)stringForProximity:(CLProximity)proximity {
    switch (proximity) {
        case CLProximityUnknown:    return @"Unknown";
        case CLProximityFar:        return @"Far";
        case CLProximityNear:       return @"Near";
        case CLProximityImmediate:  return @"Immediate";
        default:
            return nil;
    }
}

- (void)activateDoor {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://192.168.2.32" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDoorActivated object:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDoorFailed object:self];
        
    }];
    
}

@end
